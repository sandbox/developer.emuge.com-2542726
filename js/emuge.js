;
var app = (function (app, $) {

	/******** private functions & vars **********/

	// cache dom elements accessed multiple times
	function initUiCache() {
		app.ui = {
			tapsNav	: $('.tap-refinements'),
			tapsResults : $('#tap-results'),
			thisUrl : app.util.getUri(location.href)
		};

	}

	function initializeEvents() {

	}

	function initializeDom() {

		app.tapsNav.init();

	}

	// _app object
	// "inherits" app object via $.extend() at the end of this Self-Executing Anonymous Function
	var _app = {

		init: function () {

			// init global cache
			initUiCache();

			// init global dom elements
			initializeDom();

			// init global events
			initializeEvents();

		}

	};

	return $.extend(app, _app);

}(window.app = window.app || {}, jQuery));


// app.util
(function (app, $) {

	app.util = {

		// appends the parameter with the given name and
		// value to the given url and returns the changed url
		appendParamToURL : function (url, name, value) {
			var c = "?";
			if(~url.indexOf(c)) {
				c = "&";
			}
			return url + c + name + "=" + encodeURIComponent(value);
		},

		appendParamsToUrl : function (url, params) {
			var uri = app.util.getUri(url),
				includeHash = arguments.length < 3 ? false : arguments[2];

			var qsParams = $.extend(uri.queryParams, params);
			var result = uri.path+"?"+$.param(qsParams);
			if (includeHash) {
				result+=uri.hash;
			}
			if (result.indexOf("http") < 0 && result.charAt(0)!=="/") {
				result="/"+result;
			}

			return result;
		},

		removeParamFromURL : function (url, parameter) {

			var urlparts = url.split('?');

			if(urlparts.length >= 2) {
				var urlBase = urlparts.shift();
				var queryString = urlparts.join("?");
				var newQueryString = "";
				var prefix = encodeURIComponent(parameter) + '=';
				var pars = queryString.split(/[&;]/g);
				for ( var i = 0; i < pars.length; i++) {
					var thisParam = pars[i].split("=");
					if(thisParam[0] !== parameter) {
						newQueryString = newQueryString + thisParam[0] + "=" + thisParam[1] + "&";
					}
				}
				url = urlBase + "?" + newQueryString;
				if( url[url.length-1] === "&") {
					url = url.substring(0, url.length - 1);
				}
				if( url[url.length-1] === "?") {
					url = url.substring(0, url.length - 1);
				}
			}
			return url;
		},

		getQueryStringParams : function (qs) {
			if(!qs || qs.length === 0) { return {}; }

			var params = {};
			// Use the String::replace method to iterate over each
			// name-value pair in the string.
			qs.replace( new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
						function ( $0, $1, $2, $3 ) {	params[ $1 ] = $3; }
			);
			return params;
		},

		getUri : function (o) {
			var a;
			if (o.tagName && $(o).attr("href")) {
				a = o;
			}
			else if (typeof o === "string") {
				a = document.createElement("a");
				a.href = o;
			}
			else {
				return null;
			}

            // fix IE bug
            if(a.pathname.substring(0,1) == "/") {
           		var pathname = a.pathname.substr(1);
           	}
           	else {
           		var pathname = a.pathname;
           	}

			return {
				protocol : a.protocol, //http:
				host : a.host, //www.myexample.com
				hostname : a.hostname, //www.myexample.com'
				port : a.port, //:80
				path : a.pathname, // /sub1/sub2
				query : a.search, // ?param1=val1&param2=val2
				queryParams : app.util.getQueryStringParams( a.search ),
				hash : a.hash, // #OU812,5150
				url : a.protocol+ "//" + a.host + a.pathname,
				urlWithQuery : a.protocol+ "//" + a.host + "/" + pathname + a.search
			};
		}

	};

}(window.app = window.app || {}, jQuery));


//app.tapsNav
// ------------------------------------------------------------

(function (app, $) {

    app.tapsNav = {
        init : function () {

			function updateRefinements(field, value){
				// remove the page var regardless
				var url = app.util.removeParamFromURL( app.ui.thisUrl.urlWithQuery , 'page');
				// first remove this filter if it's there
				url = app.util.removeParamFromURL( url , field );
				if(value){
					var url = app.util.appendParamToURL( url , field, value );
				}
				location.href = url;
			}

			function removeRefinements(field) {
				field+='|page';
				var vars = field.split("|");
				var url = app.ui.thisUrl.urlWithQuery;
				for(i=0;i<vars.length;i++){
					url = app.util.removeParamFromURL( url , vars[i] );
				}
				location.href = url;
			}

			// loop through query params and set UI active
			for (var field in app.ui.thisUrl.queryParams) {

			   var value = decodeURIComponent(app.ui.thisUrl.queryParams[field]);
			   var fieldHeader = app.ui.tapsNav.find(".header-"+field).addClass('refined');

			   if(field === "mg"){
			   	  if (value === "one") {
			   	  	app.ui.tapsNav.find(".refinement-mg div[data-refinement-value='"+value+"']")
			   	  		.addClass('active active-mg').prepend(' + ');
			   	  }else{
			   	  	app.ui.tapsNav.find(".refinement-mg div[data-refinement-value='"+value+"']")
			   	  		.parent().parent().siblings('div')
			   	  		.addClass('active active-mg').append('<span>'+value+'</span>').prepend(' + ')
			   	  		.parent().parent().siblings('div')
			   	  			.addClass('active active-mg').prepend(' + ');
			   	  }
			   }

			   if(field === "tp") {
			   		app.ui.tapsNav.find(".refinement-tp div[data-refinement-value='"+value+"']")
			   			.hide()
			   			.parent().parent().siblings('div')
			   				.addClass('active active-tp')
			   				.append('<span>'+value+'</span>')
			   				.prepend(' + ');
			   }

			   if(field === "ts") {
				  app.ui.tapsNav.find(".refinement-ts, .header-ts").remove();
				  app.ui.tapsNav.find(".active-tp span").append(" "+value);
			   }

			   if(field === "tpi") {
					app.ui.tapsNav.find(".refinement-tpi, .header-tpi").remove();
					app.ui.tapsNav.find(".active-tp span").append(" "+value);
			   }

			}

			app.ui.tapsNav.find(".refinement-block-header").click(function(){
				var field = $(this).attr('id');
				if( $(this).hasClass('refined') ){
			   		if($(this).hasClass('header-tp')){
			   			removeRefinements('tp|ts|tpi');
			   		}
			   		else {
			   			removeRefinements(field);
			   		}
				}
			});


			// handle click on most single-value refinement links
			app.ui.tapsNav.find('.refine-link').click(function(){
				updateRefinements($(this).attr('data-refinement-field'), $(this).attr('data-refinement-value'));
			});

			// search results pagination needs same action
			app.ui.tapsResults.find('.refine-link').click(function(){
				updateRefinements($(this).attr('data-refinement-field'), $(this).attr('data-refinement-value'));
			});

			// handle toggle on sub groups groups
			app.ui.tapsNav.find('.refinement-group-header')
				.click(function(){
					if( $(this).siblings('ul:visible').size() > 0 ) {
						$(this).parent().removeClass('active').siblings().removeClass('active').parent().find('ul').hide('fast');
					}
					else {
						$(this).parent().addClass('active').siblings().removeClass('active').parent().find('ul').hide('fast');
						$(this).siblings('ul').show('fast');
					}
				})
			.siblings('ul')
				.hide();

        }
    };

}(window.app = window.app || {}, jQuery));


(function ($, Drupal, window, document, undefined) {

	jQuery(document).ready(function($){
		app.init();
	});

})(jQuery, Drupal, this, this.document);
